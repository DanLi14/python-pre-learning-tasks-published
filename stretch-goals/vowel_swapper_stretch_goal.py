def vowel_swapper(string):
    # ==============
    # Your code here
   a_vowel_counter = 0
   e_vowel_counter = 0
   i_vowel_counter = 0
   o_vowel_counter = 0
   u_vowel_counter = 0
   new_string = ''
   for let in string:
        if let == 'a' or let == 'A':
            a_vowel_counter += 1
        if a_vowel_counter == 2 and let == 'A':
            let = let.replace('A', '4')
        if a_vowel_counter == 2 and let == 'a':
            let = let.replace('a', '4')
        if let == 'e' or let == 'E':
            e_vowel_counter += 1
        if e_vowel_counter == 2 and let == 'E':
            let = let.replace('E', '3')
        if e_vowel_counter == 2 and let == 'e':
            let = let.replace('e', '3')
        if let == 'i' or let == 'I':
            i_vowel_counter += 1
        if i_vowel_counter == 2 and let == 'I':
            let = let.replace('I', '!')
        if i_vowel_counter == 2 and let == 'i':
            let = let.replace('i', '!')
        if let == 'o' or let == 'O':
            o_vowel_counter += 1
        if o_vowel_counter == 2 and let == 'O':
            let = let.replace('O', '000')
        if o_vowel_counter == 2 and let == 'o':
            let = let.replace('o', 'ooo')
        if let == 'u' or let == 'U':
            u_vowel_counter += 1
        if u_vowel_counter == 2 and let == 'U':
            let = let.replace('U', '|_|')
        if u_vowel_counter == 2 and let == 'u':
            let = let.replace('u', '|_|')
        new_string += let
   return new_string
    # ==============

print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
