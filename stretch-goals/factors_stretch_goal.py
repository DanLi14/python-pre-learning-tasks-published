def factors(number):
    # ==============
    # Your code here
    list = []
    factorList = []
    for num in range(2,number):
        list.append(num)
    for num in list:
        if number % num == 0:
            factorList.append(num)
        else:
            prime = (f"{number} is a prime number")
    if len(factorList)>0:
        return factorList
    else:
        return prime
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
